import Vue from 'vue'
import App from './App.vue'

export function createApp () {
  const app = new Vue({ // eslint-disable-line no-new
    el: '#app',
    template: '<App />',
    components: { App },
    render: (h) => h(App)
  })
  return {app}
}
