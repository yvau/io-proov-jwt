export default (payload) => {
  /*
   * We change the value of the furnised
   */
  if (payload === false) {
    return 'urgency_regular'
  } else if (payload === true) {
    return 'urgency_in_rush'
  } else {
    return ''
  }
}
