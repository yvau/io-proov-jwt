import SockJS from 'sockjs-client' // <1>
import Stomp from 'stompjs' // <2>
// import cookies from 'browser-cookies'
const link = (process.env.NODE_ENV === 'development') ? 'http://localhost:3000/server/ws' : '/ws'

function register (registrations) {
  const socket = new SockJS(link) // <3>
  const stompClient = Stomp.over(socket)
  stompClient.connect({}, frame => {
    registrations.forEach(registration => { // <4>
      stompClient.subscribe(registration.route, registration.callback)
    })
  })
}

export default {
  register
}
