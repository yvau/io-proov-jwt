/* ============
 * Main File
 * ============
 *
 * Will initialize the application.
 */

import Vue from 'vue'

/* ============
 * Plugins
 * ============
 *
 * Import and bootstrap the plugins.
 */

import 'plugins/vuex'
import 'plugins/axios'
import { i18n } from 'plugins/vue-i18n'
import { router } from 'plugins/vue-router'
import 'plugins/vuex-router-sync'
import 'plugins/multiselect'
import 'plugins/validator'
import 'plugins/font-awesome'

/* ============
 * Styling
 * ============
 *
 * Import the application styling.
 * Stylus is used for this boilerplate.
 *
 * If you don't want to use Stylus, that's fine!
 * Replace the stylus directory with the CSS preprocessor you want.
 * Import the entry point here & install the webpack loader.
 *
 * It's that easy...
 *
 * http://stylus-lang.com/
 */

import 'assets/scss/vendor.sass'
import 'assets/scss/app.sass'
import 'assets/scss/app1.sass'
import 'assets/scss/pages.sass'

/* ============
 * Main App
 * ============
 *
 * Last but not least, we import the main application.
 */

import App from 'App'
import store from 'store'

Vue.config.productionTip = false

store.dispatch('auth/check')

export function createApp () {
/* eslint-disable no-new */
  const app = new Vue({

    /**
     * The localization plugin.
     */
    i18n,

    /**
     * The router.
     */
    router,

    /**
     * The Vuex store.
     */
    store,

    /**
     * Will render the application.
     *
     * @param {Function} h Will create an element.
     */
    mounted () {
      console.log('fkjdgh')
    },
    render: h => h(App)
  })

  return { app }
}
