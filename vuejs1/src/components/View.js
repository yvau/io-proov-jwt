import routes from 'plugins/router'
export default {
  functional: true,
  data: function () {
    return {
      currentRoute: window.location.pathname
    }
  },
  computed: {
    ViewComponent () {
      const matchingView = routes[this.currentRoute]
      return matchingView
        ? require('pages/' + matchingView + '/Index').default
        : require('pages/404').default
    }
  },
  render (_, ctx) {
    const h = ctx.parent.$createElement
    return h(require('pages/About/Index').default)
  }
}
window.addEventListener('popstate', () => {
  this.a.data().currentRoute = window.location.pathname
})
