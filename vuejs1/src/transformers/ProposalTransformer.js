/* ============
 * Proposal Transformer
 * ============
 *
 * The transformer for the proposal.
 */

import Transformer from './Transformer'
import Util from 'utils/'

export default class ProposalTransformer extends Transformer {
  /**
   * Method used to transform a fetched proposal.
   *
   * @param proposal The fetched proposal.
   *
   * @returns {Object} The transformed proposal.
   */
  static fetch (proposal) {
    console.log(Util.getUrgent(proposal.urgent))
    return {
      isFurnished: Util.getFurnished(proposal.isFurnished),
      urgent: Util.getUrgent(proposal.urgent),
      priceMinimum: proposal.priceMinimum,
      priceMaximum: proposal.priceMaximum,
      bedrooms: proposal.bedrooms,
      bathrooms: proposal.bathrooms,
      status: proposal.status,
      size: proposal.size,
      ageOfProperty: proposal.ageOfProperty,
      typeOfProperty: proposal.typeOfProperty.split(',')
    }
  }

  /**
   * Method used to transform a send proposal.
   *
   * @param proposal The proposal to be send.
   *
   * @returns {Object} The transformed proposal.
   */
  static send (proposal) {
    return {
      urgent: Util.setUrgent(proposal.urgent),
      isFurnished: Util.setFurnished(proposal.isFurnished),
      bathrooms: proposal.bathrooms,
      bedrooms: proposal.bedrooms,
      location: (proposal.location === null) ? null : proposal.location.map(function (x) { return x.id }).toString(),
      typeOfProposal: proposal.typeOfProposal,
      status: proposal.status,
      ageOfProperty: proposal.ageOfProperty,
      priceMinimum: proposal.priceMinimum,
      priceMaximum: proposal.priceMaximum,
      size: proposal.size,
      typeOfProperty: proposal.typeOfProperty.toString()
    }
  }
}
