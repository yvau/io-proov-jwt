/* ============
 * Role Transformer
 * ============
 *
 * The transformer for the role.
 */

import Transformer from './Transformer'

export default class RoleTransformer extends Transformer {
  /**
   * Method used to transform a fetched account.
   *
   * @param account The fetched account.
   *
   * @returns {Object} The transformed account.
   */
  static fetch (data) {
    return {
      role: data.role.toString().toLowerCase().replace(new RegExp('role_', 'g'), '').split(',')
    }
  }

  /**
   * Method used to transform a send role.
   *
   * @param role The role to be send.
   *
   * @returns {Object} The transformed role.
   */
  static send (data) {
    return {
      role: 'ROLE_' + data.role.toString().toUpperCase().replace(new RegExp(',', 'g'), ',ROLE_')
    }
  }
}
