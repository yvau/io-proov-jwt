const routerServer = (fn) => {
  var router = {
    'home': require('pages/Home').default,
    'about': require('pages/About').default,
    'default': require('pages/404').default
  }
  return (router[fn] || router['default'])
}

export default routerServer
