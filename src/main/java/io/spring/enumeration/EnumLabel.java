package io.spring.enumeration;

/**
 * Specifies class used to gather enum label
 * (typeSale, typeRent, gender...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class EnumLabel {

    /**
     * gender enum
     */
    public enum Gender {
        male, female
    }

    /**
     * gender enum
     */
    public enum BestWayToReach {
        email, home, office
    }


}
