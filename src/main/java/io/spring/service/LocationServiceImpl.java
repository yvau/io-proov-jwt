package io.spring.service;

import io.spring.constant.Numeric;
import io.spring.model.Location;
import io.spring.model.Proposal;
import io.spring.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Specifies class LocationService used to handle methods related to
 * <code>Location Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    LocationRepository repository;

    @Override
    public void save(Location location) {
        repository.saveAndFlush(location);
    }

    /**
     *
     * @param locationList
     */
    @Override
    public void delete(List<Location> locationList) {
        List<Location> asList = new ArrayList<>();
        locationList.forEach(ile ->asList.add(ile));
        Proposal proposal = locationList.get(Numeric.NULL_VALUE).getProposals().get(Numeric.NULL_VALUE);
        proposal.getLocations().removeAll(locationList);
        repository.delete(locationList);
        repository.delete(asList);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Optional<Location> getById(final String id) {
        return Optional.ofNullable(repository.findOne(id));
    }
}
