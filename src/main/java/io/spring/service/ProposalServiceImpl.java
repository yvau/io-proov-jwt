package io.spring.service;

import io.spring.model.Proposal;
import io.spring.repository.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

/**
 * Specifies class ProposalService used to handle methods related to
 * <code>Proposal Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service("proposalService")
public class ProposalServiceImpl implements ProposalService {

    @Autowired
    private ProposalRepository repository;

    @Override
    public void save(@Param("proposal") Proposal proposal) {
        //
        repository.saveAndFlush(proposal);
        return;
    }

    @Override
    public Proposal getById(final Long id) {
        return repository.findOne(id);
    }

    @Override
    public Proposal getByIdAndTypeOfProposal(final Long id, final String typeOfProposal) {
        return repository.findByIdAndTypeOfProposal(id, typeOfProposal);
    }

    @Override
    public Page<Proposal> findAll(Proposal proposal, Pageable pageable) {
        return repository.findAll(proposal, pageable);
    }
}
