package io.spring.service;

import io.spring.model.SearchProposal;

import java.util.Optional;


/**
 * Specifies class SearchProposalService used to handle methods related to
 * <code>SearchProposal Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface SearchProposalService {

	void save(SearchProposal searchProposal);

	Optional<SearchProposal> getById(final Long id);

}
