package io.spring.service;

import io.spring.model.ProfileInformation;

import java.util.Optional;

/**
 * Specifies class ProfileInformationService used to handle methods related to
 * <code>ProfileInformation Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
public interface ProfileInformationService {

	void save(ProfileInformation profileInformation);

	Optional<ProfileInformation> getById(final Long id);

}
