package io.spring.service;

import io.spring.model.AutoIncrement;
import io.spring.repository.AutoIncrementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class AutoIncrementServiceImpl implements AutoIncrementService {

    private final AutoIncrementRepository repository;

    @Autowired
    public AutoIncrementServiceImpl(final AutoIncrementRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param autoIncrement
     */
    @Override
    public void save(final AutoIncrement autoIncrement) {
        repository.save(autoIncrement);
        return;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Optional<AutoIncrement> getById(final String id) {
        return Optional.ofNullable(repository.findOne(id));
    }
}
