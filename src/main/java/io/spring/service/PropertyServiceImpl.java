package io.spring.service;

import com.querydsl.core.BooleanBuilder;
import io.spring.model.Property;
import io.spring.model.QProperty;
import io.spring.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

/**
 * Specifies class PropertyService used to handle methods related to
 * <code>Property Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service("propertyService")
public class PropertyServiceImpl implements PropertyService {

    @Autowired
    PropertyRepository repository;

    /**
     *
     * @param property for saving later
     */
    @Override
    // @PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_LESSOR') and #property?.profile?.credential == authentication?.name")
    public void save(@Param("property") Property property) {
        repository.saveAndFlush(property);
        return;
    }

    /**
     *
     * @param id
     * @return optional for get
     */
    @Override
    public Property getById(final Long id) {
        return repository.findOne(id);
    }

    /**
     *
     * @param property
     * @param pageable
     * @return repository for property
     */
    @Override
    public Page<Property> findAll(Property property, Pageable pageable) {
        return repository.findAll(where(property), pageable);
    }

    /**
     *
     * @param property
     * @return boolean builder for filtering property list
     */
    private BooleanBuilder where(Property property) {
        QProperty qProperty = QProperty.property;
        BooleanBuilder where = new BooleanBuilder();
        if (property.getSaleType() != null) {
            where.and(qProperty.saleType.toLowerCase().eq(property.getSaleType().toLowerCase()));
        }
        if (property.getPrice() != null) {
            where.and(qProperty.saleType.toLowerCase().eq(property.getSaleType().toLowerCase()));
        }
        return where;
    }
}
