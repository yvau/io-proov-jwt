package io.spring.service;

import io.spring.model.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Specifies class ProfileService used to handle methods related to
 * <code>Profile Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface ProfileService {

	void save(Profile profile);

	Optional<Profile> getByCredential(final String credential);

	Optional<Profile> getByToken(final String token);

	Optional<Profile> getById(final Long id);

	Page<Profile> getAll(Profile profile, Pageable pageable);

}
