package io.spring.service;

import io.spring.model.PropertyPhoto;
import io.spring.repository.PropertyPhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Specifies class PropertyPhotoService used to handle methods related to
 * <code>PropertyPhoto Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class PropertyPhotoServiceImpl implements PropertyPhotoService {

    private final PropertyPhotoRepository repository;

    @Autowired
    public PropertyPhotoServiceImpl(final PropertyPhotoRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param propertyPhoto
     * @return
     */
    public PropertyPhoto save(PropertyPhoto propertyPhoto) {
        return repository.saveAndFlush(propertyPhoto);
    }

    /**
     *
     * @param id
     * @return
     */
    public PropertyPhoto find(Long id) {
        return repository.findOne(id);
    }
}
