package io.spring.security;

import io.spring.model.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(Profile user) {
        return new JwtUser(
                user.getCredential(),
                user.getPassword(),
                user.getEnabled(),
                user.getAccountNonExpired(),
                user.getCredentialsNonExpired(),
                user.getAccountNonLocked(),
                getAuthorities(user.getRole())
        );
    }

    /**
     *
     * @param role
     * @return
     */
    private static List<String> getRolesAsList(String role) {
        List<String> rolesAsList = new ArrayList<>();
        String[] arrayRole = role.split(",");
        for(String roles : arrayRole){
            rolesAsList.add(roles);
        }
        return rolesAsList;
    }

    /**
     * @param roles
     * @return
     */
    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    /**
     * @param role
     * @return
     */
    private static List<GrantedAuthority> getAuthorities(String role) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRolesAsList(role));
        return authList;
    }
}
