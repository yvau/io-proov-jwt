package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the package_product database table.
 * 
 */
@Entity
@Table(name="package_product")
@NamedQuery(name="PackageProduct.findAll", query="SELECT p FROM PackageProduct p")
public class PackageProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private Integer count;

	private String name;

	private BigDecimal price;

	@Column(name="price_tva")
	private BigDecimal priceTva;

	private String type;

	//bi-directional many-to-one association to Payment
	@OneToMany(mappedBy="packageProduct")
	private List<Payment> payments;

	public PackageProduct() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCount() {
		return this.count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPriceTva() {
		return this.priceTva;
	}

	public void setPriceTva(BigDecimal priceTva) {
		this.priceTva = priceTva;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Payment> getPayments() {
		return this.payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public Payment addPayment(Payment payment) {
		getPayments().add(payment);
		payment.setPackageProduct(this);

		return payment;
	}

	public Payment removePayment(Payment payment) {
		getPayments().remove(payment);
		payment.setPackageProduct(null);

		return payment;
	}

}