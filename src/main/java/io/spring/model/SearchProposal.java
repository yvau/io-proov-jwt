package io.spring.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the search_proposal database table.
 * 
 */
@Entity
@Table(name="search_proposal")
@NamedQuery(name="SearchProposal.findAll", query="SELECT s FROM SearchProposal s")
public class SearchProposal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="age_of_property")
	private String ageOfProperty;

	@Column(name="is_first_buyer")
	private Boolean isFirstBuyer;

	@Column(name="is_pre_approved")
	private Boolean isPreApproved;

	@Column(name="is_pre_qualified")
	private Boolean isPreQualified;

	@Column(name="is_urgent")
	private Boolean isUrgent;

	@Column(name="type_of_proposal")
	private String typeOfProposal;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to SearchProposalDetail
	@OneToMany(mappedBy="searchProposal")
	private List<SearchProposalDetail> searchProposalDetails;

	public SearchProposal() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgeOfProperty() {
		return this.ageOfProperty;
	}

	public void setAgeOfProperty(String ageOfProperty) {
		this.ageOfProperty = ageOfProperty;
	}

	public Boolean getIsFirstBuyer() {
		return this.isFirstBuyer;
	}

	public void setIsFirstBuyer(Boolean isFirstBuyer) {
		this.isFirstBuyer = isFirstBuyer;
	}

	public Boolean getIsPreApproved() {
		return this.isPreApproved;
	}

	public void setIsPreApproved(Boolean isPreApproved) {
		this.isPreApproved = isPreApproved;
	}

	public Boolean getIsPreQualified() {
		return this.isPreQualified;
	}

	public void setIsPreQualified(Boolean isPreQualified) {
		this.isPreQualified = isPreQualified;
	}

	public Boolean getIsUrgent() {
		return this.isUrgent;
	}

	public void setIsUrgent(Boolean isUrgent) {
		this.isUrgent = isUrgent;
	}

	public String getTypeOfProposal() {
		return this.typeOfProposal;
	}

	public void setTypeOfProposal(String typeOfProposal) {
		this.typeOfProposal = typeOfProposal;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<SearchProposalDetail> getSearchProposalDetails() {
		return this.searchProposalDetails;
	}

	public void setSearchProposalDetails(List<SearchProposalDetail> searchProposalDetails) {
		this.searchProposalDetails = searchProposalDetails;
	}

	public SearchProposalDetail addSearchProposalDetail(SearchProposalDetail searchProposalDetail) {
		getSearchProposalDetails().add(searchProposalDetail);
		searchProposalDetail.setSearchProposal(this);

		return searchProposalDetail;
	}

	public SearchProposalDetail removeSearchProposalDetail(SearchProposalDetail searchProposalDetail) {
		getSearchProposalDetails().remove(searchProposalDetail);
		searchProposalDetail.setSearchProposal(null);

		return searchProposalDetail;
	}

}