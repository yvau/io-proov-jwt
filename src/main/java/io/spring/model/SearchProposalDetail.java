package io.spring.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the search_proposal_details database table.
 * 
 */
@Entity
@Table(name="search_proposal_details")
@NamedQuery(name="SearchProposalDetail.findAll", query="SELECT s FROM SearchProposalDetail s")
public class SearchProposalDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="price_maximum")
	private BigDecimal priceMaximum;

	@Column(name="price_minimum")
	private BigDecimal priceMinimum;

	@Column(name="type_of_property")
	private String typeOfProperty;

	//bi-directional many-to-one association to Location
	@ManyToOne(cascade = CascadeType.ALL)
	private Location location;

	//bi-directional many-to-one association to SearchProposal
	@ManyToOne
	@JoinColumn(name="search_proposal_id")
	private SearchProposal searchProposal;

	public SearchProposalDetail() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPriceMaximum() {
		return this.priceMaximum;
	}

	public void setPriceMaximum(BigDecimal priceMaximum) {
		this.priceMaximum = priceMaximum;
	}

	public BigDecimal getPriceMinimum() {
		return this.priceMinimum;
	}

	public void setPriceMinimum(BigDecimal priceMinimum) {
		this.priceMinimum = priceMinimum;
	}

	public String getTypeOfProperty() {
		return this.typeOfProperty;
	}

	public void setTypeOfProperty(String typeOfProperty) {
		this.typeOfProperty = typeOfProperty;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public SearchProposal getSearchProposal() {
		return this.searchProposal;
	}

	public void setSearchProposal(SearchProposal searchProposal) {
		this.searchProposal = searchProposal;
	}

}