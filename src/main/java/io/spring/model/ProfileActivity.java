package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the profile_activity database table.
 * 
 */
@Entity
@Table(name="profile_activity")
@NamedQuery(name="ProfileActivity.findAll", query="SELECT p FROM ProfileActivity p")
public class ProfileActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Column(name="is_read")
	private Boolean isRead;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name="profile_id")
	private Profile profile1;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name="profile_sender_id")
	private Profile profile2;

	//bi-directional many-to-one association to ProfileActivityDetail
	@OneToMany(mappedBy="profileActivity")
	private List<ProfileActivityDetail> profileActivityDetails;

	public ProfileActivity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Boolean getIsRead() {
		return this.isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public Profile getProfile1() {
		return this.profile1;
	}

	public void setProfile1(Profile profile1) {
		this.profile1 = profile1;
	}

	public Profile getProfile2() {
		return this.profile2;
	}

	public void setProfile2(Profile profile2) {
		this.profile2 = profile2;
	}

	public List<ProfileActivityDetail> getProfileActivityDetails() {
		return this.profileActivityDetails;
	}

	public void setProfileActivityDetails(List<ProfileActivityDetail> profileActivityDetails) {
		this.profileActivityDetails = profileActivityDetails;
	}

	public ProfileActivityDetail addProfileActivityDetail(ProfileActivityDetail profileActivityDetail) {
		getProfileActivityDetails().add(profileActivityDetail);
		profileActivityDetail.setProfileActivity(this);

		return profileActivityDetail;
	}

	public ProfileActivityDetail removeProfileActivityDetail(ProfileActivityDetail profileActivityDetail) {
		getProfileActivityDetails().remove(profileActivityDetail);
		profileActivityDetail.setProfileActivity(null);

		return profileActivityDetail;
	}

}