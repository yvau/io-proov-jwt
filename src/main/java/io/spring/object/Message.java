package io.spring.object;

/**
 * Specifies class Message used to handle methods related to
 * <code>Error and message</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 5/9/2017
 */
public class Message {
    private String field;
    private String message;
    private Object object;

    public Message(String field, String message, Object object) {
        this.field = field;
        this.message = message;
        this.object = object;
    }

    public String getField() {
        return field;
    }
    public String getMessage() {
        return message;
    }
    public Object getObject() {
        return object;
    }
}
