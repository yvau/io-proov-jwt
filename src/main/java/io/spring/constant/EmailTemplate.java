package io.spring.constant;

/**
 * Specifies class used to gather multiples variables constant related to
 * <code>EmailTemplate</code>
 * (EMAIL_REGISTRATION, EMAIL_RECOVERING...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class EmailTemplate {


    public static final String EMAIL_REGISTRATION = "registration-mail";
    public static final String EMAIL_RECOVERING = "recover-mail";
    /**
     * Prevent instantiation
     */
    private EmailTemplate() {
    }
}
