package io.spring.validator;

import io.spring.object.PropertyObject;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Specifies class PropertyValidator used to handle methods related to
 * <code>Property validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Component
public class PropertyValidator implements Validator {

    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return PropertyObject.class.isAssignableFrom(c);
    }


    @Override
    public void validate(Object obj, Errors errors) {

        // PropertyObject property = (PropertyObject) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "saleType",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "size",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type",
                "is.required");

        /*if(property.getBedrooms() != null) {
            if(property.getBedrooms() < 0){
                errors.rejectValue("bedrooms", "is.incorrect");
            }
        }

        if(property.getBathrooms() != null ) {
            if(property.getBathrooms() < 0){
                errors.rejectValue("bathrooms", "is.incorrect");
            }
        }

        if (!property.getSaleType().isEmpty()) {
            if(property.getSaleType().equals(VariableConstant.FOR_SALE)){
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bedrooms",
                        "is.required");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bathrooms",
                        "is.required");
            }
            try {
                boolean found = false;
                if (property.getSaleType().equals(VariableConstant.FOR_SALE)) {
                    for (typeSale type : typeSale.values()) {
                        if (property.getType().equals(type.toString())) {
                            found = true;
                        }
                    }
                    if (!found) errors.rejectValue("type", "is.incorrect");
                }
                if (property.getSaleType().equals(VariableConstant.FOR_RENT)) {
                    for (typeRent type : typeRent.values()) {
                        if (property.getType().equals(type.toString())) {
                            found = true;
                        }
                    }
                    if (!found) errors.rejectValue("type", "is.incorrect");
                }
            } catch (Exception e) {
               // errors.rejectValue("type", "is.required");
            }
        }

        if (!String.valueOf(property.getPrice()).isEmpty()) {
            if (property.getPrice().doubleValue() < 0) {
                errors.rejectValue("price", "is.incorrect");
            }
        }

        if (!String.valueOf(property.getSize()).isEmpty()) {
            if (property.getSize().doubleValue() < 0) {
               errors.rejectValue("size", "is.incorrect");
            }
        }*/




    }

}
