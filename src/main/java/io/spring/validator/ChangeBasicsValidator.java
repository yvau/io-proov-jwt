package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.enumeration.EnumLabel.Gender;
import io.spring.model.ProfileInformation;
import io.spring.object.CredentialPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Specifies class ChangeBasicsValidator used to handle methods related to
 * <code>Basic validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 5/9/2017
 */
@Component
public class ChangeBasicsValidator implements Validator {

    @Autowired
    DateValidator dateValidator;

    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return ProfileInformation.class.isAssignableFrom(c);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        CredentialPassword credentialPassword = (CredentialPassword) obj;

        /**
         * check if the important values as lastName, gender and birthDate are filled up
         */
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName",
                "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender",
                "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate",
                "is.required");

        /**
         * check if the value entered by the user is
         * first check if in the correct format dd/MM/yyyy
         * second check if the birthdate is in the past
         */
        if (!Strings.isNullOrEmpty(credentialPassword.getBirthDate())) {
            if (!dateValidator.isThisDateValid(credentialPassword.getBirthDate())) {
                errors.rejectValue("birthDate", "date.incorrect");
            } else if (dateValidator.isThisDateValid(credentialPassword.getBirthDate()) && dateValidator.isDateInFuture(credentialPassword.getBirthDate())) {
                errors.rejectValue("birthDate", "date.incorrect");
            }
        }

        /**
         * check if value entered by the user is either female or male
         */
        if (!Strings.isNullOrEmpty(credentialPassword.getGender())) {
            List<String> enumNames = Stream.of(Gender.values())
                    .map(Enum::name)
                    .collect(Collectors.toList());

            if (!enumNames.contains(credentialPassword.getGender())) {
                errors.rejectValue("gender", "gender.incorrect");
            }
        }

    }
}
