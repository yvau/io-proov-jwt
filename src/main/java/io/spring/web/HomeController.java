package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

import static io.spring.common.utils.State.populateModel;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/16/2017
 */

@Controller
public class HomeController {

    /**
     *
     * @return
     */
    @GetMapping("/")
    public String home(Model model, HttpServletRequest request) {
        populateModel(model, request);
        return "index";
    }

    @GetMapping("/about/")
    public String aboutUs(Model model, HttpServletRequest request) {
        populateModel(model, request);
        return "index";
    }

    @GetMapping("/vone-advantage/")
    public String advantage(ModelMap model, HttpServletRequest request) {
        model.addAttribute("url", "pages/About");
        return "index";
    }

    @GetMapping("/relationship")
    public String relationship() {

        return "index";
    }

    @GetMapping("/social")
    public String social() {

        return "index";
    }


}
