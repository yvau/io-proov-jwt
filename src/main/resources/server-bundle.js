(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__awaitServer__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App__ = __webpack_require__(2);
/* eslint-disable no-undef */

// import router from './router/routerServer'

global.renderApp = function (_viewName, model, url) {
  var assetManifest = {
  "app.css": "static/css/app.151d0ac848b618eb5f4d13eea814d7b9.css",
  "app.js": "static/js/app.3c61a8536333c0de08be.js",
  "app.js.map": "static/js/app.3c61a8536333c0de08be.js.map",
  "manifest.js": "static/js/manifest.e724d3e7631180a292cc.js",
  "manifest.js.map": "static/js/manifest.e724d3e7631180a292cc.js.map",
  "static/fonts/fontawesome-webfont.eot": "static/fonts/fontawesome-webfont.674f50d.eot",
  "static/fonts/fontawesome-webfont.eot?v=4.7.0": "static/fonts/fontawesome-webfont.674f50d.eot",
  "static/fonts/fontawesome-webfont.ttf?v=4.7.0": "static/fonts/fontawesome-webfont.b06871f.ttf",
  "static/fonts/fontawesome-webfont.woff2?v=4.7.0": "static/fonts/fontawesome-webfont.af7ae50.woff2",
  "static/fonts/fontawesome-webfont.woff?v=4.7.0": "static/fonts/fontawesome-webfont.fee66e7.woff",
  "static/img/fontawesome-webfont.svg?v=4.7.0": "static/img/fontawesome-webfont.912ec66.svg",
  "vendor.js": "static/js/vendor.be9a38889c432ec8a032.js",
  "vendor.js.map": "static/js/vendor.be9a38889c432ec8a032.js.map"
};
  var cssPath = 'static/css/main.css';
  var manifest = 'static/js/manifest.js';
  var vendor = 'static/js/vendor.js';
  var app = 'static/js/app.js';

  if (assetManifest) {
    manifest = assetManifest['manifest.js'];
    vendor = assetManifest['vendor.js'];
    app = assetManifest['app.js'];
    cssPath = assetManifest['app.css'];
  }

  var requestPath = getData(model);
  console.log(requestPath);

  function populateTemplate(markup) {
    return '<!doctype html>\n    <html>\n  <head>\n    <link rel=\'stylesheet\' href=\'/' + cssPath + '\' />\n  </head>\n  <body>\n    <div id=\'app\'>' + markup + '</div>\n    <script type=\'text/javascript\' src=\'https://code.jquery.com/jquery-2.1.4.min.js\'></script>\n    <script type=\'text/javascript\' src=\'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\'\n     integrity=\'sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS\' crossorigin=\'anonymous\'></script>\n   <script type=\'text/javascript\' src=\'/' + manifest + '\'></script>\n   <script type=\'text/javascript\' src=\'/' + vendor + '\'></script>\n   <script type=\'text/javascript\' src=\'/' + app + '\'></script>\n </body>\n</html>';
  }

  // const serializer = (new Packages.com.fasterxml.jackson.databind.ObjectMapper()).writer()

  function getData(model) {
    var renderData = { data: {} };
    for (var key in model) {
      if (key.startsWith('__')) {
        renderData[key.substring(2)] = model[key];
      } else {
        renderData.data[key] = model[key];
      }
    }

    /* Serialise the model for passing to the client. We don't use JSON.stringify
     * because Nashorn's version doesn't cope with POJOs by design.
     *
     * http://www.slideshare.net/SpringCentral/serverside-javascript-with-nashorn-and-spring
     */
    // renderData.json = serializer.writeValueAsString(renderData.data)

    /* "Purify" the model by swapping it for the serialised version */
    // renderData.data = JSON.parse(renderData.json)

    return renderData;
  }

  var vm = new Vue({
    template: '<App/>',
    components: { App: __WEBPACK_IMPORTED_MODULE_1__App__["a" /* default */] }
  });

  var inWait = Object(__WEBPACK_IMPORTED_MODULE_0__awaitServer__["a" /* default */])(function (done) {
    renderVueComponentToString(vm, function (err, res) {
      done(err, res);
    });
  });

  if (inWait.error) {
    console.log(inWait.error);
  } else {
    return populateTemplate(inWait.result);
  }
};

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* eslint-disable no-undef */
var awaitServer = function awaitServer(fn) {
  var Phaser = Java.type('java.util.concurrent.Phaser');
  var phaser = new Phaser();
  var results = { error: null, result: '' };
  phaser.register();

  var done = function done(err, res) {
    if (err) {
      results.error = err;
    } else {
      results.result = res;
    }
    phaser.arriveAndDeregister();
  };

  phaser.register();
  setTimeout(function () {
    fn(done);
  }, 0);

  phaser.awaitAdvanceInterruptibly(phaser.arrive());
  return results;
};

/* harmony default export */ __webpack_exports__["a"] = (awaitServer);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_App_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_02bc78d2_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_App_vue__ = __webpack_require__(5);
var normalizeComponent = __webpack_require__(3)
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = "cdeef530"
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_App_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_02bc78d2_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_App_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 3 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//

/* ============
 * Entry Point
 * ============
 *
 * The entry point of the application
 */
// import websocketListener from 'utils/websocket-listener'
/* harmony default export */ __webpack_exports__["a"] = ({
  /**
   * The name of the application.
   */
  name: 'proov-app',
  /*
      methods: {
        subscription: function () {
          return websocketListener.register([
            {route: '/topic/greetings', callback: this.getConsole}
          ])
        },
        getConsole: function () {
          console.log('test')
        }
      }, */
  /**
   * Fires when the app has been mounted.
   */
  mounted: function mounted() {
    // If the user is authenticated,
    // fetch the data from the API
    if (this.$store.state.auth.authenticated) {
      // this.subscription()
      this.$store.dispatch('account/find');
    }
  }
});

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{attrs:{"id":"app"}},[_c('router-view')],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ })
/******/ ]);
});
//# sourceMappingURL=server-bundle.js.map