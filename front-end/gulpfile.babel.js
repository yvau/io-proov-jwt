var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var babelify = require('babelify');
var vueify = require('vueify');
var aliases = require('aliasify');
var envify = require('envify');
var gutil = require('gulp-util');
var path = require('path');
var exec = require('gulp-exec');

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

var aliasifyConfig = {
    aliases: {
        '@': './src'
    },
    verbose: true
}

gulp.task('default', () => {
  browserify({
    entries: './src/entry-server.js',
    debug: false,
    standalone: 'server',
    extensions: ['.js', '.vue', '.json'],
    // defining transforms here will avoid crashing your stream
	transform: [[babelify], [vueify], [envify, {'global': true, NODE_ENV: 'production'}], [aliases, aliasifyConfig]]
  })
  .bundle()
  .on('error', err => {
    gutil.log("Browserify Error", gutil.colors.red(err.message))
  })
  .pipe(source('server-bundle.js'))
  .pipe(gulp.dest('./dist/build'))
  .pipe(exec('cat ./dist/build/server-bundle.js ./src/return-render-function.js > ../src/main/resources/render/index.js'))
});