import 'babel-polyfill';
import { createApp } from './main';
import {createRenderer} from 'vue-server-renderer';
import awaitServer from './awaitServer';
const { renderToString } = createRenderer();

global.renderApp = function () {

  // var data = Java.from(comments);

  function populateTemplate(markup) {
      return "<!doctype html>\n<html>\n  <head>\n    <title>Hello, world!</title>\n    <meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\" integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">\n  </head>\n  <body>\n    <div id=\"root\">" + markup + "</div>\n    <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\n    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js\" integrity=\"sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh\" crossorigin=\"anonymous\"></script>\n    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js\" integrity=\"sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ\" crossorigin=\"anonymous\"></script>\n </body>\n</html>";
    }

/*var vm = new Vue({
  template: '<h1>{{ message }}</h1>',
  data: {
    message: 'Hello Vue.js!'
  }
});*/

  var results = awaitServer(function (done) {
    const vm = new createApp();
    renderToString(vm, function (err, res) {
      done(err, res);
    });
  });

  if (results.error) {
  	console.log(createApp())
    console.log(results.error);
  } else {
  	return populateTemplate(results.result);
  }

  // Spring calls the function with the view name and data, but we don't
  // use the former
  // return populateTemplate('result');
};
