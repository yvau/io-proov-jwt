/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  // Home
  {
    path: '/home',
    name: 'home.index',
    component: require('@/pages/Home/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true
    }
  },

  // Account
  {
    path: '/account',
    name: 'account.index',
    component: require('@/pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      auth: true
    }
  },

  // Login
  {
    path: '/login',
    name: 'login.index',
    component: require('@/pages/Login/Index'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  // Register
  {
    path: '/register',
    name: 'register.index',
    component: require('@/pages/Register/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true
    }
  },

  {
    path: '/',
    redirect: '/home'
  },

  {
    path: '/*',
    redirect: '/home'
  }
]
