import 'babel-polyfill'
import { createApp } from './main'

// client-specific bootstrapping logic...

const { app } = createApp()

console.log(app)

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
