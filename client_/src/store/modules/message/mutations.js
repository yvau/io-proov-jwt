/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import {
  FLASHMESSAGE
} from './mutation-types'

export default {
  [FLASHMESSAGE] (state, payload) {
    state.display = payload.status
    state.object = payload
  }
}
