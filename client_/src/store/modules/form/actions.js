/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import store from '@/store'
import Proxy from '@/proxies/Proxy'
import service from '@/services'
import * as types from './mutation-types'

export const submit = ({ commit }, payload) => {
  console.log('payload.data')
  console.log(payload.data)
  new Proxy()
     .submit(payload.method, payload.url, payload.data)
     .then((response) => {
       try {
         service.errorMessage(response)
       } catch (err) {
         response.status = true
         store.dispatch('message/flashMessage', response)
       }
     })
     .catch(() => {
       console.log('Request failed...')
     })
}

export const find = ({ commit }, payload) => {
  new Proxy(payload.url, {})
     .find(payload.data)
     .then((response) => {
       try {
         commit(types.FORMOBJ, payload.transformer.fetch(response))
       } catch (err) {
         console.log(err)
       }
     })
     .catch(() => {
       console.log('Request failed...')
     })
}

export default {
  submit,
  find
}
