/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the auth module.
 */

export const FORMOBJ = 'FORMOBJ'

export default {
  FORMOBJ
}
