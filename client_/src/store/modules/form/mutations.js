/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import {
  FORMOBJ
} from './mutation-types'

export default {
  [FORMOBJ] (state, payload) {
    state.formObj = payload
  }
}
