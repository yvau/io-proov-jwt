/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */
import {
  DISPLAY
} from './mutation-types'

export default {
  [DISPLAY] (state, payload) {
    state.images.push(payload)
  }
}
