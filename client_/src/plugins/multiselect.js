/* ============
 * Vue Multiselect
 * ============
 *
 * Multiselect
 *
 * http://monterail.github.io/vue-multiselect/
 */

import Vue from 'vue'
import Multiselect from 'vue-multiselect'

// register globally
Vue.component('multiselect', Multiselect)
