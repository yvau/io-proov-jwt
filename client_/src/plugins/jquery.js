/* ============
 * jQuery
 * ============
 *
 * Require jQuery-slim
 *
 * https://github.com/thesabbir/jquery-slim
 */
import jQuery from 'jquery-slim'

window.$ = window.jQuery = jQuery
