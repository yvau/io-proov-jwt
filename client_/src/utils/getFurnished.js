export default (payload) => {
  /*
   * We change the value of the furnised
   */
  if (payload === false) {
    return 'not_furnished'
  } else if (payload === true) {
    return 'is_furnished'
  } else {
    return ''
  }
}
