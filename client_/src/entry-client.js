import 'babel-polyfill'
import { createApp } from './main'
import './plugins/font-awesome'
import './plugins/jquery'
import './plugins/pace'
import 'vue-multiselect/dist/vue-multiselect.min.css'

// client-specific bootstrapping logic...

const { app } = createApp()

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
