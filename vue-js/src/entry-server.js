/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable wrap-iife */
import { CreateApp } from './main'
import awaitServer from './awaitServer'

global.renderApp = function () {
  // var data = Java.from(comments)

  function populateTemplate (markup) {
    return `<!doctype html>
<html lang="en">
  <head>
    <title>Hello, world!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
    ${markup}

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script type=text/javascript src=/static/js/manifest.e355efa66c5370e361b9.js></script>
  <script type=text/javascript src=/static/js/vendor.ddfada805419c84266e1.js></script>
  <script type=text/javascript src=/static/js/app.3dd2eef4d88fbb6f8713.js></script>
  </body>
</html>`;
  }

  var results = awaitServer(function (done) {
    const vm = new CreateApp()
    renderToString(vm, function (err, res) {
      done(err, res)
    })
  })

  if (results.error) {
    console.log(results.error)
  } else {
    return populateTemplate(results.result)
  }

  // Spring calls the function with the view name and data, but we don't
  // use the former
  // return populateTemplate('result')
}
