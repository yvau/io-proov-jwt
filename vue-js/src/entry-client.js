import { CreateApp } from './main'

// client-specific bootstrapping logic...

const { app } = CreateApp()

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
