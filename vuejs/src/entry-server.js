/* eslint-disable no-undef */
import App from './App'
import awaitServer from './awaitServer'
global.renderApp = function () {
  function populateTemplate (markup) {
    return '<!doctype html>\n<html lang=\'en\'>\n  <head>\n    <title>Hello, world!</title>\n    <!-- Required meta tags -->\n    <meta charset=\'utf-8\'>\n    <meta name=\'viewport\' content=\'width=device-width, initial-scale=1, shrink-to-fit=no\'>\n\n    <!-- Bootstrap CSS -->\n    <link rel=\'stylesheet\' href=\'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\' integrity=\'sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\' crossorigin=\'anonymous\'>\n  </head>\n  <body>\n    ' + markup + '\n\n    <!-- Optional JavaScript -->\n    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\n    <script src=\'https://code.jquery.com/jquery-3.2.1.slim.min.js\' integrity=\'sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\' crossorigin=\'anonymous\'></script>\n    <script src=\'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js\' integrity=\'sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh\' crossorigin=\'anonymous\'></script>\n    <script src=\'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js\' integrity=\'sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ\' crossorigin=\'anonymous\'></script>\n  </body>\n</html>'
  }

  /* var vm = new Vue({
    template: '<div>{{ msg }}</div>',
    data: {
      msg: 'hello'
    }
  }) */

  /* var vm = new Vue({
    el: '#app',
    render: h => h(App)
  }) */

  function createApp () {
    const app = new Vue({
      // the root instance simply renders the App component.
      render: h => h(App)
    })
    return app
  }

  var results = awaitServer(function (done) {
    renderVueComponentToString(createApp(), function (err, res) {
      done(err, res)
    })
  })

  if (results.error) {
    console.log(results.error)
  } else {
    return populateTemplate(results.result)
  }
}
