/* ============
 * Plugins
 * ============
 *
 * Import and bootstrap the plugins.
 */
import Vue from 'vue'

import { i18n } from './plugins/vue-i18n'
import { router } from './plugins/vue-router'
import store from './store'
import './plugins/vuex'
import './plugins/axios'
import './plugins/vuex-router-sync'
import './plugins/bootstrap'
import './plugins/font-awesome'
import App from './App'

/* eslint-disable no-new */
new Vue({
  /**
   * Bind the Vue instance to the HTML.
   */
  el: '#app',

  /**
   * The localization plugin.
   */
  i18n,

  /**
   * The router.
   */
  router,

  /**
   * The Vuex store.
   */
  store,

  /**
   * Will render the application.
   *
   * @param {Function} h Will create an element.
   */
  render: h => h(App)
})
// const { app } = CreateApp()

// this assumes App.vue template root element has `id="app"`
// app.$mount('#app')
